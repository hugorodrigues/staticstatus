"use strict";

const VERSION = "1.0.0.0";
const CONFIG_PATH = "/static/config/config.json";

var configuration = undefined;
var loadObjects = {
    "config": false,
    "status": false,
    "history": false
}

function waitForConfig(fn) {
    if (configuration === undefined) {
        setTimeout(waitForConfig, 500, fn);
    } else {
        fn();
    }
}

var clearLoadingTimer = undefined;

function clearLoading(action) {
    if (action !== undefined) {
        loadObjects[action] = true;
    }
    var clear = true;
    for (const rec in loadObjects) {
        if (!loadObjects[rec]) {
            clear = false;
            break
        }
    }
    if (clear) {
        document.getElementById("loading").classList.add("hidden");
        if (clearLoadingTimer !== undefined) {
            window.clearInterval(clearLoadingTimer);
        }
    }
}
clearLoadingTimer = window.setInterval(clearLoading, 100);

function loadJson(uri, callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", uri, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4) {
            if (xobj.status == 200) {
                callback(JSON.parse(xobj.responseText));
            } else {
                console.log("Unable to complete request");
                var loading = document.getElementById("loading");
                Array.from(loading.getElementsByTagName("span")).forEach(function(node, idx) {
                    node.remove();
                });
                loading.style.backgroundColor = "rgba(255, 255, 255, 1)";
                loading.classList.remove("hidden");
                var alertlist = loading.getElementsByTagName("ul")[0];
                if (alertlist === undefined) {
                    alertlist = document.createElement("ul");
                    loading.appendChild(alertlist);
                }
                var alrt = document.createElement("li");
                alrt.classList.add("alert");
                alrt.classList.add("danger");
                alrt.innerText = `Unable to load ${uri}`;
                var icon = document.createElement("i");
                icon.classList.add("icon");
                icon.classList.add("icon-stop");
                icon.classList.add("pull-left");
                alrt.appendChild(icon);
                alertlist.appendChild(alrt);
            }
        }
    };
    xobj.send(null);
}


function toggleGroup(group, changeicon) {
    if (changeicon === undefined) changeicon = true;
    var icon = group.children[0].children[0];
    var services = group.children[1];
    var newicon = undefined;
    if (services.classList.contains("hidden")) {
        services.classList.remove("hidden");
        newicon = "icon-minus-circled";
    } else {
        newicon = "icon-plus-circled";
        services.classList.add("hidden");
    }

    if(changeicon) {
        icon.classList = newicon;
    }
}

function formatDate(date) {
    var msec = Date.parse(date);
    return strftime(configuration["date_format"], new Date(msec));
}

function getStatusIcon(state) {
    var icon;
    switch (state) {
        case "resolved":
            icon = "icon-ok-circle";
            break;
        case "watching":
            icon = "icon-eye";
            break;
        case "acknowledged":
            icon = "icon-eye";
            break;
        case "warning":
            icon = "icon-attention";
            break;
        default:
            icon = "icon-attention";
            break
    }
    return icon;
}

function loadConfig(config) {
    // custom CSS
    var style = document.createElement("style");
    style.type = "text/css";
    style.innerHTML = `
    .banner {
        background-color: ${config["style"]["banner"]["background_color"]};
    }

    .banner a {
        color: ${config["style"]["banner"]["link_color"]};
    }

    .group {
        background-color: ${config["style"]["groups"]["header"]["background_color"]};
        color: ${config["style"]["groups"]["header"]["color"]};
    }

    .group div li {
        background-color: ${config["style"]["groups"]["items"]["background_color"]};
        color: ${config["style"]["groups"]["items"]["color"]};
    }

    .group div li.issue {
        background-color: ${config["style"]["groups"]["items"]["problem_background_color"]};
        color: ${config["style"]["groups"]["items"]["problem_color"]};
    }
    `;
    if (!config["show_service_link"]) {
        style.innerHTML += ".svclink {display: none;}"
    }

    document.getElementsByTagName("head")[0].appendChild(style);

    // Set company details
    var logo = document.getElementById("logo");
    logo.src = config["company"]["logo"];
    logo.alt = config["company"]["name"];
    var company = document.getElementById("company");
    company.innerText = config["company"]["name"];
    company.href = config["company"]["homepage"];
    if (config["show_homepage"]) {
        var homepage = document.getElementById("homepage");
        homepage.href = config["company"]["homepage"];
        homepage.classList.remove("hidden");
    }
    document.getElementsByTagName("title")[0].text = `${config["company"]["name"]} - Status`;

    var manual = document.getElementById("manualnotice");
    if (config["show_manual_notice"]) {
        manual.classList.remove("hidden");
        if (config["reload"] == 0) {
            manual.children[0].innerText += " Refresh to get the latest status.";
        }
    }

    if (config["show_powered"]) {
        document.getElementById("powered").classList.remove("hidden");
    }

    configuration = config;
    clearLoading("config");
}

function loadStatus(config) {

    // Statuses
    var all_ok = true;

    var status_dom = document.getElementById("status");
    status_dom.innerHTML = "";

    config["status"].forEach(function(group, gidx) {
        var gdom = document.createElement("ul");
        gdom.classList.add("group");
        var group_ok = true;

        // Header
        var gheader = document.createElement("li");
        gheader.classList.add("pointer");
        var gtoggle = document.createElement("i");
        gtoggle.classList.add("icon-minus-circled");
        var gtitle = document.createElement("strong");
        gtitle.innerText = group["name"];
        var gticon = document.createElement("i");
        gheader.appendChild(gtoggle);
        gheader.appendChild(gtitle);

        gheader.addEventListener("click", function() {
            toggleGroup(gdom, true);
        });
        gdom.appendChild(gheader);
        status_dom.appendChild(gdom);

        // Services
        var svc_group = document.createElement("div");

        group["services"].forEach(function(svc, sidx) {
            var svc_dom = document.createElement("li");
            var sname = document.createElement("span");
            sname.innerText = svc["name"];
            var slink = document.createElement("span");
            slink.classList.add("svclink");
            var sstatus = document.createElement("small");
            var ssicon = document.createElement("i");
            if (svc["running"]) {
                sstatus.innerText = "Running";
                ssicon.classList.add(getStatusIcon("resolved"));
            } else {
                svc_dom.classList.add("issue");
                sstatus.innerText = "With issues";
                ssicon.classList.add(getStatusIcon("warning"));
                all_ok = false;
                group_ok = false;
            }
            sstatus.classList.add("pull-right");
            sstatus.appendChild(ssicon);
            svc_dom.appendChild(sname);
            svc_dom.appendChild(slink);
            svc_dom.appendChild(sstatus);

            if (svc["link"]) {
                svc_dom.addEventListener("click", function() {
                    window.open(svc["link"]);
                });
                svc_dom.classList.add("pointer");
                slink.innerText = ` (${svc["link"]})`;
            }

            svc_group.appendChild(svc_dom);
        });

        if (group_ok) {
            gticon.classList.add(getStatusIcon("resolved"));
        } else {
            gticon.classList.add(getStatusIcon("warning"));
        }
        gticon.classList.add("pull-right");

        gtitle.appendChild(gticon);
        gdom.appendChild(svc_group);
    });


    var gstatus = document.getElementById("globalstatus");
    var gstatus_alert = gstatus.children[0];
    var favicon = undefined;
    if (all_ok) {
        gstatus_alert.classList.add("success");
        gstatus_alert.innerHTML = `<i class="${getStatusIcon("resolved")}"></i>All systems are operational`;
        favicon = '\u2705';
    } else {
        gstatus_alert.classList.add("warning");
        gstatus_alert.innerHTML = `<i class="${getStatusIcon("warning")}"></i>Some systems aren\'t operational`;
        favicon = '\u274E';
    }
    gstatus.classList.remove("hidden");

    // Mark status as loaded, since the next code isn't as important
    clearLoading("status");

    var fav = document.createElement("link");
    fav.rel = "shortcut icon";
    fav.type = "image/svg+xml";
    fav.href = `data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>${favicon}</text></svg>`;
    document.head.appendChild(fav);
}

function loadHistory(config) {
    var root = document.getElementById("incidents");
    root.innerHTML = "";
    config["history"].forEach(function(incident, iidx) {
        var iroot = document.createElement("ul");
        iroot.classList.add("group");

        // Header
        var iheader = document.createElement("li");
        iheader.addEventListener("click", function() {
            toggleGroup(iroot, false);
        });
        iheader.classList.add("pointer");
        var ititle = document.createElement("strong");
        ititle.innerText = incident["title"];
        var idate = document.createElement("small");
        idate.classList = ["pull-right"];
        idate.innerText = formatDate(incident["start"]);
        if (incident["end"]) {
            idate.innerText += " - ";
            var end = new Date(Date.parse(incident["end"]));
            if (end > new Date()) {
                var idatend = document.createElement("i");
                idatend.innerText = formatDate(incident["end"]);
                idate.appendChild(idatend);
            } else {
                idate.innerText += formatDate(incident["end"]);
            }
        }
        var istatus = document.createElement("i");
        istatus.classList.add("icon");
        istatus.classList.add(getStatusIcon(incident["status"]));
        iheader.appendChild(istatus);
        iheader.appendChild(ititle);
        iheader.appendChild(idate);

        var ibody = document.createElement("div");

        // Message
        var imsg = document.createElement("li");
        imsg.innerText = incident["message"];
        ibody.appendChild(imsg);

        // Actions

        incident["actions"].forEach(function(action, aidx) {
            var iaction = document.createElement("li");
            var iaicon = document.createElement("i");
            iaicon.classList.add("pull-left");
            iaicon.classList.add("icon");
            iaicon.classList.add(getStatusIcon(action["status"]));

            iaction.innerText = action["message"];

            var iadate = document.createElement("small");
            iadate.innerText = formatDate(action["date"]);
            iadate.classList = ["pull-right"];

            iaction.appendChild(iaicon);
            iaction.appendChild(iadate);
            ibody.appendChild(iaction);
        });

        iroot.appendChild(iheader);
        iroot.appendChild(ibody);
        root.appendChild(iroot);
    });
    clearLoading("history");
}

function doRun(force) {
    if (force === undefined) {
        force = false;
    }
    waitForConfig(function() {
        if (force || loadObjects["status"]) {
            loadObjects["status"] = false;
            loadJson(configuration["status"], loadStatus);
        }
    });

    waitForConfig(function() {
        if (configuration["show_history"]) {
            document.getElementById("history").classList.remove("hidden");
            if (force || loadObjects["history"]) {
                loadObjects["history"] = false;
                loadJson(configuration["history"], loadHistory);
            }
        } else {
            clearLoading("history");
        }
    });
}


// Load configuration as soon as possible
loadJson(CONFIG_PATH, loadConfig);

window.onload = function () {
    doRun(true);
    waitForConfig(function() {
        if (configuration["reload"] > 0) {
            window.setInterval(doRun, configuration["reload"] * 1000);
        }
    });
}
