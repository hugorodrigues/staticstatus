# StaticStatus
Static status page that's feeded by JSON. No server-side language needed to use this page.

## Configuration
Configuring the page requires 3 JSON files (or custom providers) located under static/config/.

You can check the valid formats on the docs folder.

### config.json
This is the main configuration file that personalizes the page.

### status.json
Current status of the services to display. Services are grouped.

### history.json
History of status. Can be disabled on config.json

### Single file for status and history
It's possible to use a single file for both status and history, just set the same URL for both options on config.json

### Using custom providers
Since this page is populated using JSON files, you can setup a custom service to provide the required information and have a automatic status page. Just make sure to serve the correct JSON objects.

You can also use a custom service for the config.json, just change the endpoint in /static/js/script.js. Tip, it's on the  **CONFIG_PATH** constant.

## License
The software is under the MIT license.